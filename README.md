# Open In Firefox

![Screenshot](Screenshot1.png)

This is not just another wrapper app that sends html files to Firefox. It detects the full path, preserving the ability to navigate to local resources such as other html files and media. (The [Firefox app](https://play.google.com/store/apps/details?id=org.mozilla.firefox) has to be installed first). 