package com.pattanshetti.shivanand.openinfirefox;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.util.Log;
import android.app.*;
import android.content.DialogInterface;
import android.os.*;
import android.widget.Toast;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Method;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends AppCompatActivity {

    Toast toast;
    boolean toastTimerRunning = false;
    CountDownTimer toastCountDown;

    // https://stackoverflow.com/questions/3976616/how-to-find-nth-occurrence-of-character-in-a-string
    public static int ordinalIndexOf(String str, String substr, int n) {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Open In Firefox");

        // Get intent, action and MIME type
        Intent intent = getIntent();
        Context context = getApplicationContext();
        String action = intent.getAction();
        String type = intent.getType();
        /*
        try {
            Log.d("CUSTOM:Intent", intent.toString());
            Log.d("CUSTOM:Action", action.toString());
            Log.d("CUSTOM:Type", type.toString());
        } catch (Exception e) {
        }
        */
//        if (Intent.ACTION_VIEW.equals(action) && type != null) {
//                openInFirefox(context, intent);
//        }
        if (type != null) {
            openInFirefox(context, intent);
        }
    }

    void openInFirefox(final Context context, final Intent intent) {
        Intent firefoxIntent;
        PathDecoder pathDecoder;
        String fullUsablePath;
        Toast.makeText(MainActivity.this, "Hello", Toast.LENGTH_LONG);

        // https://stackoverflow.com/questions/6758841/how-can-i-find-if-a-particular-package-exists-on-my-android-device
        final PackageManager packageManager = getPackageManager();
        Intent testIntent = packageManager.getLaunchIntentForPackage("org.mozilla.firefox");
        boolean isIntentValid;
        if (testIntent == null) {
            isIntentValid = false;
        } else {
            List<ResolveInfo> list = packageManager.queryIntentActivities(testIntent, PackageManager.MATCH_DEFAULT_ONLY);
            isIntentValid = (list.size() > 0);
        }

        if (!isIntentValid) {
            toast = Toast.makeText(MainActivity.this, "Mozilla Firefox is not installed. Please install it from the Google store", Toast.LENGTH_LONG);
            toastCountDown = new CountDownTimer(9000, 1000 /*Tick duration*/) {
                public void onTick(long millisUntilFinished) {
                    toastTimerRunning = true;
                    toast.show();
                }

                public void onFinish() {
                    toast.cancel();
                    toastTimerRunning = false;
                }
            };
            toast.show();
            // if(!toastTimerRunning) {
            toastCountDown.start();
            toastTimerRunning = true;
            // }
            return;
        }

        try {
            // https://stackoverflow.com/questions/38200282/android-os-fileuriexposedexception-file-storage-emulated-0-test-txt-exposed
            if (Build.VERSION.SDK_INT >= 24) {
                try {
                    Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                    m.invoke(null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            android.net.Uri URI = intent.getData();
            if (URI.getPath().startsWith("http") || URI.getPath().startsWith("https")) {
                firefoxIntent = new Intent(android.content.Intent.ACTION_VIEW);
                firefoxIntent.setData(URI);
            } else {
                firefoxIntent = new Intent(intent.getAction());
                pathDecoder = new PathDecoder();
                // Log.d("CUSTOM:PathforURI", intent.getData().getPath());
                fullUsablePath = pathDecoder.getPathFromURI(URI);
                firefoxIntent.setDataAndType(Uri.parse(fullUsablePath), intent.getType());
                // Log.d("CUSTOM:ParsedPath", fullUsablePath);
                if (!new File(fullUsablePath.replaceFirst("file:///", "/")).exists()) {
                    throw new FileNotFoundException("The file \"" + fullUsablePath + "\" does not exist.");
                }
                // startActivity(Intent.createChooser(firefoxIntent, "Open file using:"));
            }
            firefoxIntent.setPackage("org.mozilla.firefox");
            startActivity(firefoxIntent);
            this.finish();
        } catch (final Exception e) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(getApplicationContext());
            }
            builder.setTitle("Open In Firefox: Error")
                    .setMessage("We're sorry!  Opening an HTML from this app is not supported yet. " +
                            "Help us to add support for your use case by sending us this feedback message. " +
                            "Thank you!\n\n" + e.getMessage())
                    .setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.this.finish();
                        }
                    }).setPositiveButton("Send to Support", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                    String fullUsablePath = new PathDecoder().getPathFromURI(intent.getData());
                    // emailIntent.setDataAndType(Uri.parse("mailto:"), "message/rfc822");
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"shivanand.pattanshetti@gmail.com"});
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[SUPPORT] Open In Firefox");
                    emailIntent.putExtra(Intent.EXTRA_TEXT,
                            "Howdy Developer," + "\n"
                                    + "\n" + "Error Message: " + e.getMessage()
                                    + "\n" + "Received Path: " + intent.getData().getPath()
                                    + "\n" + "Parsed Path: " + fullUsablePath
                                    + "\n" + "File exists: " + new File(fullUsablePath.replaceFirst("file:///", "/")).exists()
                                    + "\n\n"
                    );
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        Toast.makeText(MainActivity.this, "Once the email client opens, please click on send.", Toast.LENGTH_LONG).show();
                        startActivity(emailIntent);
                        MainActivity.this.finish();
                    } else {
                        Toast.makeText(MainActivity.this, "Could not send the email. There are no email clients installed.", Toast.LENGTH_LONG).show();
                        MainActivity.this.finish();
                    }
                            /*
                            try {
                                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(MainActivity.this, "Could not send the email. There are no email clients installed.", Toast.LENGTH_LONG).show();
                                MainActivity.this.finish();
                            } catch(Exception e){
                                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                MainActivity.this.finish();
                            }
                            */
                }
            }).setIcon(android.R.drawable.ic_dialog_alert).show();
        }
    }

    public void onChangeDefaultBrowser(android.view.View currentView) {
        Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://"));
        android.content.pm.ResolveInfo resolveInfo = getPackageManager().resolveActivity(browserIntent, android.content.pm.PackageManager.MATCH_DEFAULT_ONLY);
        String packageName = resolveInfo.activityInfo.packageName;
        if (Build.VERSION.SDK_INT >= 24) {
            if (packageName.equalsIgnoreCase("com.pattanshetti.shivanand.openinfirefox")) {
                toast = Toast.makeText(MainActivity.this, "OpenInFirefox is already your default browser.", Toast.LENGTH_LONG);
            } else {
                toast = Toast.makeText(MainActivity.this, "Please set the default browser here.", Toast.LENGTH_LONG);
                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } else if (!packageName.equalsIgnoreCase("android")) {
            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", packageName, null));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            toast = Toast.makeText(MainActivity.this, "Please clear defaults for the current browser (" + packageName + ") here. You may need to scroll down and/or check advanced settings", LENGTH_LONG);
        } else {
            toast = Toast.makeText(MainActivity.this, "No action required right now. Please select OpenInFirefox as your default browser when opening your file/web-page", LENGTH_LONG);
        }
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(9000, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                toastTimerRunning = true;
                toast.show();
            }

            public void onFinish() {
                toast.cancel();
                toastTimerRunning = false;
            }
        };
        toast.show();
        // if(!toastTimerRunning) {
        toastCountDown.start();
        toastTimerRunning = true;
        // }

        /*
        Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://"));
        android.content.pm.ResolveInfo resolveInfo = getPackageManager().resolveActivity(browserIntent, android.content.pm.PackageManager.MATCH_DEFAULT_ONLY);
        String packageName = resolveInfo.activityInfo.packageName;
        if(packageName == null || packageName.equalsIgnoreCase("")){
            Toast.makeText(MainActivity.this, "Please set the default browser on the upcoming screen.", Toast.LENGTH_LONG).show();
            // Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", getPackageName(), null));
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else if(packageName.equalsIgnoreCase("com.pattanshetti.shivanand.openinfirefox")){
            Toast.makeText(MainActivity.this, "OpenInFirefox is already your default browser.", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(MainActivity.this, "Please clear defaults for the current browser on the upcoming screen.", Toast.LENGTH_LONG).show();
            // Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", packageName, null));
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_DEFAULT_APPS_SETTINGS);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        */
    }
}
